## Vorlage für Markdown-Artikel

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/db-kongress-uebungsaufgabe</span>

[comment]: # (Den oberen und den unteren Teil kopieren)


> **tl/dr;** _(ca. 0 min Lesezeit): Das ist nur eine Vorlage für mich, damit ich mir die häufig genutzten Markdownelemente merken kann..._

<span class="kann-liste">

Nach Durchlesen des Artikels kann ich...

- ... aus bekanntem Code/Kontrollfluss für gegebene Testfälle die Überdeckungsmetriken für Anweisungen, Zweige, Pfade und Bedingungen berechnen / bestimmen.

- ... aus Werten einer Überdeckungsmetrik rückschließen, was daraus für die anderen Überdeckungsmetriken bedeutet.

- ... eine Testsuite so ergänzen, dass die Tests den Code nach einer bestimmten Metrik (möglichst) komplett abdecken.

- ... erklären, warum es für die gleiche Funktion unterschiedliche Werte für dieselbe Metrik geben kann.

</span>

### Formatierung in Markdown
```
`[external_html_code ttl=600 url="https://oer-informatik.gitlab.io/datenformate/xml/xml-einstieg-uebung.WPHTML"]`
```
Ich kann _kursiv_, **fett** oder **_fett und kursiv_** schreiben.

- Mit Spiegelstrichen gehen Aufzählungen
  - Auch mit Unterpunkten
  - und zwar mehreren
- Mit vielen Punkten
 - mit Leerzeichen statt Tabs
 - klappen die Unterpunkte auch
- Aber immer an die Leerzeilen vor und nach der Liste denken, sonst klappt das manchmal nicht

Alternative:

* Auch mit Asterisk gehen Aufzählungen
  * Auch mit Unterpunkten
  * und zwar mehreren
* Mit vielen Punkten
 * mit Leerzeichen
 * kann man auch Unterpunkte machen
* Aber immer an die Leerzeilen vor und nach der Liste denken, sonst klappt das manchmal nicht


Man kann auch durchnummerieren:

1. Ohne
2. die
2. richtigen
3. Zahlen
1. zu
1. schreiben.

### Mathematische Ausdrücke über LaTeX-Notation:

Innerhalb des Texts lassen sich Variablen wie $x_2$ so einfügen, ganze Formelausdrücke gehen in neuen Zeilen mit:

$$R_{2} = \frac{1}{2} \Omega$$

$$9{,}81 \mathrm{\frac{m}{s^2}}$$

$$ W_{primär} = \frac{\eta}{W_g}$$


### Bilder, Links und Fußnoten

In einem Text kann [ein normaler link](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/sql-dml-selektion-projektion.html) stehen.

Text kann auch eine Fussnote enthalten, in der auch ein Link stehen kann.^[Fussnote mit Link: [https://www.jacoco.org/jacoco/trunk/doc/counters.html](https://www.jacoco.org/jacoco/trunk/doc/counters.html)]

Bilder werden am Besten in einer neuen Zeile geschrieben, damit es zu keinen Renderproblemen kommt:

![ein Bild mit normaler Bildunterschrift](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/image/select-railroad-projektion-selektion.png)

![ein Bild mit normaler Bildunterschrift mit [link](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/sql-dml-selektion-projektion.html) und Fussnote^[Fussnote mit Link: [https://www.jacoco.org/jacoco/trunk/doc/counters.html](https://www.jacoco.org/jacoco/trunk/doc/counters.html)]](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/image/select-railroad-projektion-selektion.png)


<span class="hidden-text">

Wird derzeit nicht geparst: Bilder bin Höhen oder Breiten beschränkung.

![Höhenbegrenztes Bild](https://oer-informatik.gitlab.io/python-basics/erste-schritte-mit-python/images/tdd.png){#id .class max-height=30%}

![Breitenbegrenztes Bild](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/image/select-railroad-projektion-selektion.png){#id .class max-width=30%}

</span>



### Kommentare und versteckter Text

Es gibt viele Wege für versteckten Text. Einer ist die eigene CSS-Klasse `hidden-text`. Diese wird auch bei den Fragen/Antworten (unten) genutzt. <span class="hidden-text">nochmal versteckter Text0</span>

Markdown-Kommentare finden ihren Weg gar nicht ins HTML wie z.B. dieser hier:

[comment]: # (Niemand wird je erfahren, was hier steht)

### Tabellen

| Rechts | Links | Mittig |Default |
|--------:|:-------|:--------:|---------|
|123 |abcdefg | | |
|12 |fgh 1 |i x |1234|
| xxxx |123 | xx |1 |
Table: Eine Linientabelle (Mein Standard)

--------------------------------------------
Deutsch   Englisch   Schwedisch  Hexadezimal
--------- ---------- ----------- -----------
rot       red        röd         `ff0000 `
grün      green      grön        `00ff00 `
blau      blue       blau         `0000ff`
--------- ---------- ----------- -----------
Deutsch   Englisch   Schwedisch  Hexadezimal
--------- ---------- ----------- -----------
rot       red        röd         `ff0000 `
--------- ---------- ----------- -----------
Table: Farben in deutsch , englisch, schwedisch sowie hexadezimal

------------------------ ---------------------------------------
Mehrzeilige Tabellen
------------------------ ---------------------------------------
`-u u` oder              gibt den Benutzernamen an.
`--user=u`               Standardmäßig gilt der aktuelle 
                         Benutzername bzw. unter Windows                         
                         `ODBC `.
`-h h` oder              gibt den Hostnamen des MySQL
`--host=h`               -Servers an (standardmäßig 
                         *localhost*).
`-p` oder `--password`   zeigt eine Passwortabfrage an.

`-p` oder `--password`   zeigt eine Passwortabfrage an.
------------------------ ----------------------------------------
Table: Farben in deutsch , englisch, schwedisch sowie hexadezimal


### Versteckte Antworten

1) Was versteht man unter Testgetriebener Entwicklung (TDD)? <button onclick="toggleAnswer('tdd1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="tdd1">

Bei Testgetriebener Entwicklung wird zunächst ein Test erstellt, der einen Teil der Spezifikation überprüft und erst im zweiten Schritt die zugehörige Implementierung, die den Test bestehen lässt.

![ein Bild mit normaler Bildunterschrift](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/image/select-railroad-projektion-selektion.png)

</span>

### Registerkarten, die altenativ angezeigt werden

Für die Tabs muss vor und hinter den ersten und letzten HTML-`<span>`-Elementen eine Leerzeile sein. Markdown versieht alles mit Absatz-Tags (`<p><span..></p>`), das CI-Script erkennt diese Tags bei der Leerzeilenschreibweise und entfernt sie.

<span class="tabrow" >

  <button class="tablink" data-tabgroup="sql" data-tabid="mariadb" onclick="openTabsByDataAttr('mariadb', 'sql')">MariaDB/MySQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="mssql" onclick="openTabsByDataAttr('mssql', 'sql')">MS SQLServer</button>
  <button class="tablink tabselected" data-tabgroup="sql" data-tabid="postgre" onclick="openTabsByDataAttr('postgre', 'sql')">PostgreSQL</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="oracle" onclick="openTabsByDataAttr('oracle', 'sql')">Oracle</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="sqlite" onclick="openTabsByDataAttr('sqlite', 'sql')">SQLite</button>
  <button class="tablink" data-tabgroup="sql" data-tabid="all" onclick="openTabsByDataAttr('all', 'sql')">_all_</button>
   <button class="tablink" data-tabgroup="sql" data-tabid="none" onclick="openTabsByDataAttr('none', 'sql')">_none_</button>

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="sql" data-tabid="none" style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mariadb" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%MySQL%'
DELETE FROM tbl
```

![ein Bild mit normaler Bildunterschrift](https://oer-informatik.gitlab.io/db-sql/dml-select-queries/image/select-railroad-projektion-selektion.png)

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="mssql" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%MSSQL%'
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="postgre">

```sql
SELECT * FROM db.table WHERE text LIKE '%PostgreSQL%'
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="oracle" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%Oracle%'
```

</span>

<span class="tabs"  data-tabgroup="sql" data-tabid="sqlite" style="display:none">

```sql
SELECT * FROM db.table WHERE text LIKE '%SQLite%'
```

</span>

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="language" data-tabid="java" onclick="openTabsByDataAttr('java', 'language')">Java</button>
   <button class="tablink" data-tabgroup="language" data-tabid="python" onclick="openTabsByDataAttr('python', 'language')">Python</button>
   <button class="tablink" data-tabgroup="language" data-tabid="all" onclick="openTabsByDataAttr('all', 'language')">_all_</button>
    <button class="tablink" data-tabgroup="language" data-tabid="none" onclick="openTabsByDataAttr('none', 'language')">_none_</button>
</span>

<span class="tabs" data-tabgroup="language" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="language" data-tabid="java">

```java
System.out.println("Hello World)";
```

</span>

<span class="tabs" data-tabgroup="language" data-tabid="python"  style="display:none">

```python
print('Hello World')
```

</span>

<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="buildTool" data-tabid="maven" onclick="openTabsByDataAttr('maven', 'buildTool')">Maven</button>
   <button class="tablink" data-tabgroup="buildTool" data-tabid="gradle" onclick="openTabsByDataAttr('gradle', 'buildTool')">Gradle</button>
   <button class="tablink" data-tabgroup="buildTool" data-tabid="all" onclick="openTabsByDataAttr('all', 'buildTool')">_all_</button>
    <button class="tablink" data-tabgroup="buildTool" data-tabid="none" onclick="openTabsByDataAttr('none', 'buildTool')">_none_</button>
</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="maven">

Maven

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="gradle"  style="display:none">

Gradle

</span>


<span class="tabrow" >
   <button class="tablink tabselected" data-tabgroup="shell" data-tabid="maven" onclick="openTabsByDataAttr('bash', 'shell')">Bash/\*nix</button>
   <button class="tablink" data-tabgroup="shell" data-tabid="gradle" onclick="openTabsByDataAttr('gradle', 'powershell')">PowerShell</button>
   <button class="tablink" data-tabgroup="shell" data-tabid="all" onclick="openTabsByDataAttr('all', 'shell')">_all_</button>
    <button class="tablink" data-tabgroup="shell" data-tabid="none" onclick="openTabsByDataAttr('none', 'shell')">_none_</button>
</span>

<span class="tabs" data-tabgroup="shell" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="bash">

Bash

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="powershell"  style="display:none">

PowerShell

</span>

### Die Pipeline reparieren:

Wenn Zeichen sich in die Markdown-Dateien eingeschlichen haben, die Pandoc nicht mag, dann kann man die z.B. mit Hilfe der Powershell entfernen:

```powershell
get-childitem *.md | select-string -pattern [^!-~\s\w] -allmatches -CaseSensitive`
```

Bei Meldung:
```
Error producing PDF.
! Package inputenc Error: Unicode character (U+2003)
(inputenc)                not set up for use with LaTeX.
```

```powershell
get-childitem *.md | select-string -pattern [\u2003] -allmatches -CaseSensitive
```
Oder in der Bash:
```bash
grep -onP "[^\x00-\x7FöüäßÜÜ]" *.md
```

### Links und weitere Informationen

- [oer-Informatik Repositories auf GitLab](https://gitlab.com/oer-informatik/)


