/**
<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-scripts-js"></script>

<style>
.tablink{width:auto;display:block;padding:8px 16px;text-align:center;border:none;white-space:normal;float:left;outline:0;color:darkblue;background-color:#BBBBBB;}
.tabrow{width:100%;display:inline-block;}
.tabselected{color:#000!important;background-color:lightblue!important;}
</style>

<span class="tabrow" >

  <button class="tablink tabselected" data-tabgroup="g1" data-tabid="opt01" onclick="openTabsByDataAttr('opt01', 'g1')">Option 1</button>
  <button class="tablink" data-tabgroup="g1" data-tabid="opt02" onclick="openTabsByDataAttr('opt02', 'g1')">Option 2</button>
  <button class="tablink" data-tabgroup="g1" data-tabid="opt03" onclick="openTabsByDataAttr('opt03', 'g1')">Option 3</button>
  <button class="tablink" data-tabgroup="g1" data-tabid="all" onclick="openTabsByDataAttr('all', 'g1')">all</button>
  <button class="tablink" data-tabgroup="g1" data-tabid="none" onclick="openTabsByDataAttr('none', 'g1')">none</button>

</span>

<span id="opt01" class="tabs"  data-tabgroup="g1" data-tabid="opt01">

Text der ersten Option

</span>

<span id="opt02" class="tabs" data-tabgroup="g1" data-tabid="opt02" style="display:none">

Im Fall der zweiten Optino erscheint dieses

</span>

<span id="opt03" class="tabs"  data-tabgroup="g1" data-tabid="opt03" style="display:none">

Und die dritte Option

</span>
*/

function toggleAnswer(elementID) {
  var answer = document.getElementById(elementID);
  if(answer.className.includes("hidden-answer")){
    answer.className = answer.className.replace("hidden-answer", "visible-answer");
  }else if(answer.className.includes("visible-answer")){
    answer.className = answer.className.replace("visible-answer", "hidden-answer");
  }
}

function openTabsByDataAttr(tabId, tabGroup="") {
  var i;
  var tabcontent = document.getElementsByClassName("tabs");
    for (i = 0; i < tabcontent.length; i++) {
  		if (tabcontent[i].getAttribute("data-tabgroup")===tabGroup){
    		if ((tabcontent[i].getAttribute("data-tabid")===tabId)||((tabId ==="all")&&(tabcontent[i].getAttribute("data-tabid")!="none"))){
          tabcontent[i].style.display = "block";
          tabcontent[i].title = tabcontent[i].getAttribute("data-tabid");
    		}else{
          tabcontent[i].style.display = "none"; 
    		}
    	}
    }
    var tabrow = document.getElementsByClassName("tablink");
    for (i = 0; i < tabrow.length; i++) {
  		if (tabrow[i].getAttribute("data-tabgroup")===tabGroup){
    		if (tabrow[i].getAttribute("data-tabid")===tabId){
          if (!tabrow[i].className.includes(" tabselected")){
            tabrow[i].className += " tabselected"; 
          }
    		}else{
          tabrow[i].className = tabrow[i].className.replace(" tabselected", "");
    		}
    	}
    }
  }
