# Wordpress-Plugin zum importieren von HTML-Blöcken

Dieses Wordpress-Plugin basiert im Wesentlichen auf dem Code von [https://github.com/pReya/wordpress-external-markdown](https://github.com/pReya/wordpress-external-markdown), lediglich der Teil, der HTML aus Markdown erzeugt wurde entfernt. Dieses Plugin steht unter der Lizenz "GPL v2 or later", das gilt somit auch für den hier geringfügig angepassten Code.

Das Plugin ist abgestimmt auf die CI-Pipeline unter https://gitlab.com/oer-informatik/service/ci-pipeline und nutzt die von ihr generierten Dateien *.WPHTML, deren Image-Pfade und Tabellenbreiten angepasst sind sowie deren unpassende HTML-Tags entfernt wurden (z.B. alles, wass bei Einbettung doppelt wäre).

Was ist zu tun?

1. Den Plugin-Ordner zippen und als Plugin in Wordpress installieren
2. In gitlab CI Pfad auf Projekt anpassen (.gitlab-ci.yml)
3. (optional) Name der Datei als hidden-Permalink editiert unter Überschrift in den Markdown-Dateien https://oer-informatik.de/hier-lang
4. Pfad auf die einzelnen Markdown-Seiten, die vom CI erzeugt wurden in Wordpress als Beitrag einfügen mit dem Shortcode:

`[external_html_code ttl=600 url="https://oer-informatik.gitlab.io/devoptools/git/git04-readme-erstellen.WPHTML"]`

4. Kategorien anpassen, Permalink wie Dateiname setzen
