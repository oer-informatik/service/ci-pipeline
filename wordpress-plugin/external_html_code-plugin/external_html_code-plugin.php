<?php
/**
 * Plugin Name: External HTML-Tags
 * Author:      Hannes Stein
 * Description: Include files from external web sources via HTTP-Request
 * Plugin URI:  no URL
 * Version:     0.0.2
 * License:     GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 *
 * Contributes:  Mainly copied from https://github.com/pReya/wordpress-external-markdown
 */

function external_html_code_shortcode($atts = array())
{
  // 1 hour = 60s * 60
  $DEFAULT_CACHE_TTL = strval(60 * 60);
  $SOURCE_OF_HTMLCODE = "https://example.org/source.WPHTML";

  extract(shortcode_atts(array(
    'url' => $SOURCE_OF_HTMLCODE,
    'class' => 'external_html_code',
    'ttl' => $DEFAULT_CACHE_TTL
  ), $atts));

  // TTL != 0 means caching is enabled
  if ($ttl !== strval(0)) {
    $cache_key = "external_html_code_" . md5($url . $class . $ttl);
    $cached = get_transient($cache_key);
  }

  // Cache miss or cache disabled
  if (!(isset($cached)) || ($cached === false)) {
    $fetch_content = wp_remote_get($url);
    $content_response_body = wp_remote_retrieve_body($fetch_content);
    $content_response_code = wp_remote_retrieve_response_code($fetch_content);

    if ($content_response_code != 200) {
      return "<strong>Plugin Error:</strong> Could not fetch external htmlcode source.";
    }

	$html_string = '<div class="' . $class . '">' . $content_response_body . '</div>';
    if ($ttl != 0) {
      set_transient($cache_key, $html_string, $ttl);
    }

    return $html_string;
  } else {
    // Cache hit
    return $cached;
  }
}

add_shortcode('external_html_code', 'external_html_code_shortcode');